# About

Here you can find the BVM (Bag Valve Mask) assembly for Autodesk Fusion.
You can use this assembly to import it in your design and start working on your mechanical concept. It has two rigid joints, two slide joints for valves and one revolve joint for the duckbill valve.


